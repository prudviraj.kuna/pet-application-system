<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Login</title>
<style>
body {
	background-color: #49b7ac;
}

h1 {
	color: white;
	text-align: center;
}

p {
	font-family: verdana;
	font-size: 20px;
}

a {
	background-color: black;
	color: white;
	padding: 1em 1.5em;
	text-decoration: none;
}
img {
  display: block;
  margin-left:auto;
  margin-right: auto;
}
</style>
</head>
<body>
	<h1>USER LOGIN PAGE</h1>
	${loginsuccess}
	<hr />
	<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="index.jsp">Home</a>
	<c:form id="loginuser" action="validate" method="post"
		modelAttribute="userlogin">
		<table align="center">
			<tr>
				<td>User Name:</td>
				<td><c:input path="userName" autocomplete="off"
						required="required"></c:input></td>
			</tr>
			<tr></tr><tr></tr>
			<tr>
				<td>Password:</td>
				<td><c:password path="password" autocomplete="off"
						required="required"></c:password></td>
			</tr>
			<tr></tr>
			<tr></tr>
			<tr>
				<td><c:button>login</c:button></td>
			</tr>
		</table>
		<br/><br/>
		<img src="https://images.pexels.com/photos/1643456/pexels-photo-1643456.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" width="100%" height="45%" align="left"/>
	</c:form>
</body>
</html>