<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>mypets</title>
<style>
body {
	background-color: #49b7ac;
}

h1 {
	color: white;
	text-align: center;
}

p {
	font-family: verdana;
	font-size: 20px;
}

a {
	background-color: black;
	color: white;
	padding: 1em 1.5em;
	text-decoration: none;
}
</style>
</head>
<body>
<c:form id="petsinsert" action="addtomypets" method="post"  >
	<table border="center">  
    <tr>  
      <th>username</th>  
      <td><input name="username" type="text" ></td>  
    </tr>  
    <tr>  
      <th>pet</th>  
      <td><input name="pet" type="text"  value="${petdetails.getPet()}"></td>  
    </tr>  
    <tr>  
      <th> pet name </th>  
      <td><input name="petname" type="text"  value="${petdetails.getPetname()}"></td>  
    </tr>  
    <tr>  
      <th>pet breed </th>  
      <td><input name="petbreed" type="text"  value="${petdetails.getPetbreed()}"></td>  
    </tr>  
    <tr>  
      <th>life span</th>  
      <td><input name="lifespan" type="text" value="${petdetails.getLifespan()}"></td>  
    </tr> 
     <tr>  
      <th>color</th>  
      <td><input name="color" type="text"  value="${petdetails.getColor()}"></td>  
    </tr> 
   
    <tr>  
      <th colspan="2"><input type="submit" value="Submit" >  
      </th>  
    </tr> 
  </table>  
  

</c:form>
</body>


</html>