<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
<style>
body {
	background-color: #49b7ac;
}

h1 {
	color: white;
	text-align: center;
}

p {
	font-family: verdana;
	font-size: 20px;
}

a {
	background-color: black;
	color: white;
	padding: 1em 1.5em;
	text-decoration: none;
}
</style>
</head>
<body>
	<h1>REGISTRATION PAGE</h1>
	<hr /><br />
	<a href="index.jsp">Home</a>
	
	<br />
	
	<c:form action="adduser" method="post" modelAttribute="user">
		<table align="center">
			<tr>
				<td>User Name:</td>
				<td><c:input path="userName" autocomplete="off"></c:input></td>
			</tr>
			<tr></tr>	<tr></tr>
			<tr>
				<td>Password:</td>
				<td><c:password path="password" autocomplete="off"></c:password></td>
			</tr>
			<tr></tr>	<tr></tr>
			<tr>
				<td><c:button>Register</c:button></td>
			</tr>
		</table>
	</c:form>
</body>
</html>