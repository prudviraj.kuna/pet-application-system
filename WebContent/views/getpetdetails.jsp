<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Buy pet</title>
<style>
body {
	background-color: #49b7ac;
}

h1 {
	color: white;
	text-align: center;
}

p {
	font-family: verdana;
	font-size: 20px;
}

a {
	background-color: black;
	color: white;
	padding: 1em 1.5em;
	text-decoration: none;
}
</style>
</head>
<body>

	<h1>You buy the below pet</h1>
	<hr/>
	<c:forEach items="${view}" var="petdetails">
	<br/><br/>
		<a
			href="mypet?pet=${petdetails.getPet()}&petname=${petdetails.getPetname()}&petbreed=${petdetails.getPetbreed()}
	&lifespan=${petdetails.getLifespan()}&color=${petdetails.getColor()}&cost=${petdetails.getCost()}">Please
			add the details to your cart</a>
			<a href="mycart">My Cart</a>
		<table align="center" border="10">
			<tr>
				<th>Pet</th>
				<td>${petdetails.getPet()}</td>
			<tr>
				<th>Pet Name</th>
				<td>${petdetails.getPetname()}</td>
			</tr>
			<tr>
				<th>Pet Breed</th>
				<td>${petdetails.getPetbreed()}</td>
			</tr>
			<tr>
				<th>Life Span</th>
				<td>${petdetails.getLifespan()}</td>
			</tr>
			<tr>
				<th>Color</th>
				<td>${petdetails.getColor()}</td>
			</tr>
			<tr>
				<th>Cost</th>
				<td>${petdetails.getCost()}</td>
			</tr>
		</table>
	</c:forEach>
</body>
</html>

