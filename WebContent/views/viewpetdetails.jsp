<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view all pets details</title>
<style>
body {
	background-color: #49b7ac;
}

h1 {
	color: white;
	text-align: center;
}

p {
	font-family: verdana;
	font-size: 20px;
}

a {
	background-color: black;
	color: white;
	padding: 1em 1.5em;
	text-decoration: none;
}
</style>
</head>
<body>
	<h1>View List of all Pets</h1>
	<hr /><br/><br/>
	<a href="index.jsp">Home</a>
	<a href="addpetsdetails">Add Pets Details</a>
	<a href="logout">Logout</a>
	<br />
	<br />
	<table border="15" align="center">
		<tr>
			<th>Pet</th>
			<th>Pet Name</th>
			<th>Pet Breed</th>
			<th>Life Span</th>
			
			<th>color</th>
			<th>cost</th>
			<th>Status</th>
		</tr>
		<c:forEach items="${view}" var="petdetails">
			<tr>
				<td>${petdetails.getPet()}</td>
				<td>${petdetails.getPetname()}</td>
				<td>${petdetails.getPetbreed()}</td>
				<td>${petdetails.getLifespan()}</td>
				
				<td>${petdetails.getColor()}</td>
				<td>${petdetails.getCost()}</td>
				<td>${petdetails.getStatus()}</td>
				<td><a href="viewbuypets?pet=${petdetails.getPet()}&petname=${petdetails.getPetname()}
				&petbreed=${petdetails.getPetbreed()}&lifespan=${petdetails.getLifespan()}
				&color=${petdetails.getColor()}&cost=${petdetails.getCost()}&status=${petdetails.getStatus()}">Buy</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
