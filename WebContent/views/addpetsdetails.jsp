<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Pets Details</title>
<style>
body {
	background-color: #49b7ac;
}

h1 {
	color: white;
	text-align: center;
}

p {
	font-family: verdana;
	font-size: 20px;
}

a {
	background-color: black;
	color: white;
	padding: 1em 1.5em;
	text-decoration: none;
}
</style>
</head>
<body>
<h1>Add Pets Details</h1>
<hr/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="index.jsp">Back</a>
<a href="logout">Logout</a>
	<c:form action="addpets" method="post" modelAttribute="pets">
		<table align="center" border="10">
			<tr>
				<td>Pet:</td>
				<td><c:input path="pet" autocomplete="off"></c:input></td>
			</tr>

			<tr>
				<td>Pet Name:</td>
				<td><c:input path="petname" autocomplete="off"></c:input></td>
			</tr>

			<tr>
				<td>Pet Breed:</td>
				<td><c:input path="petbreed" autocomplete="off"></c:input></td>
			</tr>

			<tr>
				<td>Life Span:</td>
				<td><c:input path="lifespan" autocomplete="off"></c:input></td>
			</tr>

			<tr>
				<td>Color:</td>
				<td><c:input path="color" autocomplete="off"></c:input></td>
			</tr>

			<tr>
				<td>Cost:</td>
				<td><c:input path="cost" autocomplete="off"></c:input></td>
			</tr>

			<tr>
				<td>Status:</td>
				<td><c:input path="status" autocomplete="off"></c:input></td>
			</tr>
			<tr>
				<td><c:button>Add</c:button></td>
			</tr>

		</table>
	</c:form>
</body>
</html>