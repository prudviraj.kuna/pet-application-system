package dao;

import java.util.List;

import model.Register;
import model.MyCart;
import model.PetDetails;

public interface PetDAO {
	public void userLogin(Register user);

	public boolean checkLogin(String userName, String Password);

	public List<PetDetails> viewpets();

	public void addpets(PetDetails petdetails);

	public List<PetDetails> getByPetName(PetDetails petdetails);

	public void insertIntoMyPets(MyCart mypets);

	public List<MyCart> viewmypets();

}