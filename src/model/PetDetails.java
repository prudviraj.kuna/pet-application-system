package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PetDetails 
{
	
    private String pet;
    @Id
    private String petname;
    private String petbreed;
    private String lifespan;
    private String color;
    private double cost;
    private String status;
	public String getPet() {
		return pet;
	}
	public void setPet(String pet) {
		this.pet = pet;
	}
	public String getPetname() {
		return petname;
	}
	public void setPetname(String petname) {
		this.petname = petname;
	}
	public String getPetbreed() {
		return petbreed;
	}
	public void setPetbreed(String petbreed) {
		this.petbreed = petbreed;
	}
	public String getLifespan() {
		return lifespan;
	}
	public void setLifespan(String lifespan) {
		this.lifespan = lifespan;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
